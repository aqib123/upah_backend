import {success, notFound} from '../../services/response/'
import {sendTemplate} from '../../services/mandrill'
import {PasswordReset,User} from  '../../models'
import request from 'request-promise'

//const sgMail = require('@sendgrid/mail');
// using SendGrid's v3 Node.js Library
// https://github.com/sendgrid/sendgrid-nodejs

//sgMail.setApiKey(process.env.UpahSG);

export const create = ({body: {email}}, res, next) =>
  User.findOne({email})
    .then(notFound(res))
    .then((user) => user ? PasswordReset.create({user}) : null)
    .then(async(reset) => {
      if (!reset) return null
      const {user, code} = reset;
      /*const msg = {
        to: email,
        from: 'ibtehal599@gmail.com',
        subject: 'Reset Password Request',
        html: '<strong>Your reset password code is: </strong>' + code,
      };
      console.log("=======================>", email, code)
      return sgMail.send(msg);*/

      let ress = await request({
        uri: 'https://api.elasticemail.com/v2/email/send',
        json: true,
        form: {
          apikey: process.env.ELASTICMAIL_APIKEY,
          from: 'info@upahsg.com',
          from_name: 'info@upahsg.com',
          to: email,
          username: 'info@upahsg.com',
          subject: 'Reset Password Request',
          body_html: 'Your reset password code is: <strong>' + code + '</strong>'
        }
      });
      console.log("===>",ress)
      return ress
    })
    .then(success(res))
    .catch(next);

export const show = ({params: {code}}, res, next) =>
  PasswordReset.findOne({code})
    .populate('user')
    .then(notFound(res))
    .then((reset) => reset ? reset.view(true) : null)
    .then(success(res))
    .catch(next)

export const update = ({params: {code}, bodymen: {body: {password}}}, res, next) => {
  return PasswordReset.findOne({code})
    .populate('user')
    .then(notFound(res))
    .then((reset) => {
      if (!reset) return null
      const {user} = reset
      return user.set({password}).save()
        .then(() => PasswordReset.remove({user}))
        .then(() => user.view(true))
    })
    .then(success(res))
    .catch(next)
}
