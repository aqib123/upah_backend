import {Router} from 'express'
import {login} from './controller'
import {password, master, facebook, google, windowLive, twitter, twitterCallback} from '../../services/passport'
import {User} from '../../models'
const router = new Router();
import jwt from 'jsonwebtoken'
import moment from 'moment';
import config from './../../config'
import request from 'request'
import qs  from 'querystring'
/**
 * @api {post} /auth Authenticate
 * @apiName Authenticate
 * @apiGroup Auth
 * @apiPermission master
 * @apiHeader {String} Authorization Basic authorization with email and password.
 * @apiParam {String} access_token Master access_token.
 * @apiSuccess (Success 201) {String} token User `access_token` to be passed to other requests.
 * @apiSuccess (Success 201) {Object} user Current user's data.
 * @apiError 401 Master access only or invalid credentials.
 */
router.post('/',
  password(),
  login);

/**
 * @api {post} /auth/facebook Authenticate with Facebook
 * @apiName AuthenticateFacebook
 * @apiGroup Auth
 * @apiParam {String} access_token Facebook user accessToken.
 * @apiSuccess (Success 201) {String} token User `access_token` to be passed to other requests.
 * @apiSuccess (Success 201) {Object} user Current user's data.
 * @apiError 401 Invalid credentials.
 */
router.post('/facebook',
    facebook(),
    login);

/**
 * @api {post} /auth/google Authenticate with Google
 * @apiName AuthenticateGoogle
 * @apiGroup Auth
 * @apiParam {String} access_token Google user accessToken.
 * @apiSuccess (Success 201) {String} token User `access_token` to be passed to other requests.
 * @apiSuccess (Success 201) {Object} user Current user's data.
 * @apiError 401 Invalid credentials.
 */
router.post('/google',
    google(),
    login);

router.post('/live',
    windowLive(),
    login);

function createJWT(user) {
    console.log('createJWT', user, user._id);
    return jwt.sign(user._id, config.jwtSecret);
}



export default router
