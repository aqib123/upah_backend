import {success, notFound} from '../../services/response/'
import {Charge} from '../../models'
import mongoose from 'mongoose'

export const index = ({req, user}, res, next) => {
  Charge.find({senderId: mongoose.Types.ObjectId(user._id)})
    .then(success(res, 200))
    .catch(next)
};

export const create = ({body}, res, next) => {
  Charge.create(body)
    .then(success(res, 201))
    .catch(next)
};
