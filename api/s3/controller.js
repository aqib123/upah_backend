/**
 * Created by iAboShosha on 5/6/17.
 */
import {success, notFound} from '../../services/response/'
import AWS from 'aws-sdk'
import config from '../../config'

export const index = ({bodymen:{body}, user}, res, next) => {
  console.log("================>", body);

  AWS.config.update({
    accessKeyId: config.AWS.ACCESS_KEY_ID,
    secretAccessKey: config.AWS.SECURET_ACCESS_KEY,
    region: config.AWS.REGION
  });

  const s3 = new AWS.S3({
    signatureVersion: 'v4',
  });

  const myBucket = config.AWS.BUCKET_NAME;
  const myKey = body.filename;
  const signedUrlExpireSeconds = config.AWS.DEFAULT_SIGNED_URL_EXPIRATION_IN_SECS;

  var url = s3.getSignedUrl('putObject', {
    Bucket: myBucket,
    Key: myKey,
    Expires: signedUrlExpireSeconds
  });
  res.json({url, urlBasic: config.AWS.URL_BASIC})
}

