/**
 * Created by iAboShosha on 5/6/17.
 */

import {Router} from 'express'
import {index} from './controller'

import {middleware as body} from 'bodymen'
import {token} from '../../services/passport'

const router = new Router();

router.post('/',
  body({filename: String, type: String}),
  index);

export default router
