import _ from 'lodash'
import {success, notFound} from '../../services/response/'
import { Offer } from '../../models'
import mongoose from 'mongoose'

export const showByBidder = ({params}, res, next) =>{
  Offer.findOne({bidder: mongoose.Types.ObjectId(params.id)})
    .then(notFound(res))
    .then(success(res))
    .catch(next);
};

export const showByBidderOrOwner = ({params}, res, next) =>{
  Offer.find({$or: [
    {bidder: mongoose.Types.ObjectId(params.userId)},
    {owner: mongoose.Types.ObjectId(params.userId)}
  ]}).populate('bidder').populate('owner').populate('service')
    .then(notFound(res))
    .then(success(res))
    .catch(next);
};

export const create = ({body}, res, next) => {
  body._id = body.name;
  Offer.create(body)
    .then(success(res, 201))
    .catch(next)
};

export const update = ({body, user}, res, next) =>
  Offer.findById(body._id)
    .then(notFound(res))
    .then((offer) => offer ? _.merge(offer, body).save() : null)
    .then(success(res))
    .catch(next);

export const destroy = ({params, user}, res, next) =>
  Offer.findById(params.id)
    .then(notFound(res))
    .then((offer) => offer ? offer.remove() : null)
    .then(success(res, 204))
    .catch(next);
