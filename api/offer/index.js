import {Router} from 'express'
import {middleware as query} from 'querymen'
import {middleware as body} from 'bodymen'
import {showByBidder, create, update, destroy, showByBidderOrOwner} from './controller'
import {token} from '../../services/passport'

const router = new Router();

router.get('/bidder/:id',
token({required: true}),
  showByBidder);

router.get('/:userId',
token({required: true}),
  showByBidderOrOwner);


router.post('/',
token({required: true}),
  create);


router.put('/',
token({required: true}),
  update)


router.delete('/:id',
token({required: true}),
  destroy);

export default router
