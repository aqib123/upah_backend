import {success, notFound} from '../../services/response/'
import {Withdrawal} from '../../models'
import mongoose from 'mongoose'

export const index = ({req, user}, res, next) => {

  Withdrawal.find({userId: mongoose.Types.ObjectId(user._id)})
    .then(success(res, 200))
    .catch(next)
};

export const create = ({body}, res, next) => {
  Withdrawal.create(body)
    .then(success(res, 201))
    .catch(next)
};
