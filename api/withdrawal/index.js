import {Router} from 'express'
import {middleware as query} from 'querymen'
import {middleware as body} from 'bodymen'
import {create, index} from './controller'
import {token} from '../../services/passport'

const router = new Router();

router.get('/',
  token({required: true}),
  index);

router.post('/',
  token({required: true}),
  create);

export default router
