import {success, notFound} from '../../services/response/'
import {Chat} from '../../models'

export const index = ({querymen: {query, select, cursor}}, res, next) =>
  Chat.find().limit(3700)
    .then((chats) => chats.map((chat) => chat.view()))
    .then(success(res))
    .catch(next);

export const getByConversationId = ({params}, res, next) =>
  Chat.find({'conversationId': params.conversationId}).sort({'createdAt': 1})
    .then((chats) => chats.map((chat) => chat.view()))
    .then(success(res))
    .catch(next);

export const create = ({body}, res, next) => {
  Chat.create(body)
    .then(success(res, 201))
    .catch(next)
};

export const getByUserId = ({params}, res, next) =>
  Chat.find({'receiverId': params.userId}).sort({'createdAt': 1})
    .then((chats) => chats.map((chat) => chat.view()))
    .then(success(res))
    .catch(next);
