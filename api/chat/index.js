import {Router} from 'express'
import {middleware as query} from 'querymen'
import {middleware as body} from 'bodymen'
import {index, getByConversationId, create, getByUserId} from './controller'
import {token} from '../../services/passport'
const router = new Router()

router.get('/',
  token({required: true}),
  query(),
  index);


router.get('/:conversationId',
  token({required: true}),
  getByConversationId);

router.get('byUser/:userId',
  token({required: true}),
  getByUserId);

router.post('/',
  token({required: true}),
  create);

export default router
