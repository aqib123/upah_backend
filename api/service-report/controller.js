import {success, notFound} from '../../services/response/'
import {ServiceReport} from '../../models'
import mongoose from 'mongoose'


export const create = ({body}, res, next) => {
  ServiceReport.create(body)
    .then(success(res, 201))
    .catch(next)
};
