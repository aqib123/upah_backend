import _ from 'lodash'
import {success, notFound} from '../../services/response/'
import { Service } from '../../models'
import { Offer } from '../../models'
import mongoose from 'mongoose'

export const search = (req, res, next) => {
  var query = {};
  var cursor = {};
  var select = {};
  if (req.body.query) {
    query = req.body.query
  }
  if (req.body.cursor) {
    cursor = req.body.cursor
  }
  query.$and.push({"status": {"$ne": "COMPLETED"}});
  _.forEach(query.$and, q=> {
    if (q.hasOwnProperty('category._id')) {
      q['category._id'] = mongoose.Types.ObjectId(q['category._id'])
    }
    if (q.hasOwnProperty('userId')) {
      q['userId'] = mongoose.Types.ObjectId(q['userId'])
    }
  });
  var service = Service
    .aggregate(
    [
      {
        $lookup: {
          "from": "categories",
          "localField": "categoryId",
          "foreignField": "_id",
          "as": "category"
        }
      },
      {
        $lookup: {
          "from": "users",
          "localField": "userId",
          "foreignField": "_id",
          "as": "user"
        }
      },
      {$unwind: "$user"},
      {$unwind: "$category"},
      {$match: query}
    ]
  );
  var serviceCount = Service
    .aggregate(
    [
      {$match: query}
    ]
  );
  var count = serviceCount.exec();
  var services = service.exec();
  Promise.all([services, count])
    .then(function (result) {
      res.json({
        services: result[0],
        count: result[1].length
      })
    })
    .catch(next)

};

export const index = ({querymen: {query, select, cursor}}, res, next) =>
  Service.find(query, select, cursor).populate('userId').populate('categoryId')
    .then(success(res))
    .catch(next);

export const getGive = (req, res, next) =>
  Service.find({type: "give"}).populate('userId').populate('categoryId')
    .then(success(res))
    .catch(next);

export const getTake = (req, res, next) =>
  Service.find({type: "take"}).populate('userId').populate('categoryId')
    .then(success(res))
    .catch(next);

export const show = ({params}, res, next) =>{
  Service.findOne({_id: mongoose.Types.ObjectId(params.id)})
    .then(notFound(res))
    .then(success(res))
    .catch(next);
};

export const showByUser = ({params}, res, next) =>{
  Service.findOne({userId: mongoose.Types.ObjectId(params.id)})
    .then(notFound(res))
    .then(success(res))
    .catch(next);
};

export const create = async ({body, user}, res, next) => {
  try{
    let upah;
    if(user.upah) {
      upah = user.upah
    }
    await User.findOneAndUpdate({_id: mongoose.Types.ObjectId(user._id)}, {upah: upah + 1});

  } catch(err) {console.log("=============================>", err)}
  body.userId = JSON.parse(JSON.stringify(user))._id;
  Service.create(body)
    .then(success(res, 201))
    .catch(next)
};

export const update = ({body, user}, res, next) =>
  Service.findById(body._id)
    .then(notFound(res))
    .then((service) => service ? _.merge(service, body).save() : null)
    .then(success(res))
    .catch(next);

export const destroy = ({params, user}, res, next) =>
  Service.findById(params.id)
    .then(notFound(res))
    .then((service) => service ? service.remove() : null)
    .then(success(res, 204))
    .catch(next);

export const getOffers = ({params}, res, next) =>{
  Offer.find({service: mongoose.Types.ObjectId(params.id)})
    .populate('bidder')
    .then(notFound(res))
    .then(success(res))
    .catch(next);
} ;
