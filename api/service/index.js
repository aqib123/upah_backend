import {Router} from 'express'
import {middleware as query} from 'querymen'
import {middleware as body} from 'bodymen'
import {index, show, create, update, destroy, getGive, getTake, search, showByUser, getOffers} from './controller'
import {token} from '../../services/passport'


const router = new Router();


router.get('/',
  query(),
  index);

router.post('/search',
  search);

router.get('/getGive',
  getGive);

router.get('/getTake',
  getTake);

router.get('/id/:id',
  show);

router.get('/userId/:id',
  showByUser);


router.post('/',
  token({required: true}),
  create);

router.put('/',
token({required: true}),
  update)

  router.get('/offers/:id',
    getOffers);

export default router
