import {success, notFound} from '../../services/response/'
import {Conversation} from '../../models'

export const index = ({querymen: {query, select, cursor}}, res, next) =>
  Conversation.find().limit(3700 )
    .then((conversations) => conversations.map((conversation) => conversation.view()))
    .then(success(res))
    .catch(next);

export const create = ({body}, res, next) => {
  Conversation.create(body)
    .then(success(res, 201))
    .catch(next)
};
export const getByUsersByService = ({params}, res, next) => {
  var query;
  if(params.serviceId != 'undefined') {
    console.log("====================> service exists", params)
    query = {"$or":
      [
        {"$and": [{'senderId': params.senderId}, {'receiverId': params.receiverId}, {'serviceId': params.serviceId}]},
        {"$and": [{'receiverId': params.senderId}, {'senderId': params.receiverId}, {'serviceId': params.serviceId}]}
      ]}
  } else {
    console.log("====================> service xxxxx", params)

    query = {"$or":
      [
        {"$and": [{'senderId': params.senderId}, {'receiverId': params.receiverId}]},
        {"$and": [{'receiverId': params.senderId}, {'senderId': params.receiverId}]}
      ]}
  }
  Conversation.findOne(query)
    .then(notFound(res))
    .then(success(res, 200))
    .catch(next);
}


export const show = ({params}, res, next) =>
  Conversation.findById(params.id).populate('receiverId').populate('senderId')
    .then(notFound(res))
    .then(success(res, 200))
    .catch(next);


export const del = ({params}, res, next) =>
	Conversation.deleteOne({_id: params.id }, function (err) {
  if (err) return handleError(err);
  // deleted at most one tank document
});