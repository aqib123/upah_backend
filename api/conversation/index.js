import {Router} from 'express'
import {middleware as query} from 'querymen'
import {middleware as body} from 'bodymen'
import {index, getByUsersByService, create, show,del} from './controller'
import {token} from '../../services/passport'
const router = new Router();

router.get('/',
token({required: true}),
  query(),
  index);

router.get('/:senderId/:receiverId/:serviceId',
token({required: true}),
  getByUsersByService);

router.get('/:id',
  token({required: true}),
  show);
router.post('/',
token({required: true}),
  create);

router.post('/:id/del',
token({required: true}),
  del);
  
export default router
