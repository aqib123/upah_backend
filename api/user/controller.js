import _ from 'lodash'
import {
    success,
    notFound
} from '../../services/response/'
import {
    User,
    Wallet
} from '../../models'
import {
    sendTemplate
} from '../../services/mandrill'
import {
    sign
} from '../../services/jwt'
export const index = ({
    querymen: {
        query,
        select,
        cursor
    }
}, res, next) => User.find(query, select, cursor).limit(4000).then((users) => users.map((user) => user.view(true))).then(success(res)).catch(next)
export const search = (req, res, next) => {
    var query = {};
    var cursor = {};
    var select = {};
    if (req.body.query) {
        query = req.body.query
    }
    if (req.body.cursor) {
        cursor = req.body.cursor
    }
    var user = User.aggregate(
        [{
            $match: query
        }]);
    var userCount = User.aggregate(
        [{
            $match: query
        }]);
    var count = userCount.exec()
    var users = user.exec()
    Promise.all([users, count]).then(function(result) {
        res.json({
            users: result[0],
            count: result[1].length
        })
    }).catch(next)
};
export const show = ({
    params
}, res, next) => User.findById(params.id).then(notFound(res)).then((user) => user ? user.view(true) : null).then(success(res)).catch(next)
export const showMe = ({
    user
}, res, next) => {
    User.findById(JSON.parse(JSON.stringify(user))._id).populate("wallet").then(notFound(res)).then((user) => user ? user.view(true) : null).then(success(res)).catch(next)
};
export const create = async ({
    bodymen: {
        body
    }
}, res, next) => {
    let wallet = {
        available: 0,
        given: 0,
        pending: 0,
        taken: 0
    };
    wallet = await Wallet.create(wallet);
    body.wallet = wallet._id;
    let user;
    try {
        user = await User.create(body)
    } catch (err) {
        /* istanbul ignore else */
        if (err.name === 'MongoError' && err.code === 11000) {
            res.status(409).json({
                valid: false,
                param: 'email',
                message: 'email already registered'
            })
        } else {
            next(err)
        }
    }
    sign(user._id).then((token) => {
        return ({
            token,
            user: user.view(true)
        })
    }).then(success(res, 201)).catch((err) => {
        next(err)
    })
};
export const update = (req, res, next) => {
    console.log(req.body);
    User.findById(req.body.id).then(notFound(res)).then((user) => user ? _.mergeWith(user, req.body, customizer).save() : null).then((user) => user ? user.view(true) : null).then(success(res)).catch(next)
};

function customizer(objValue, srcValue) {
    if (_.isArray(objValue)) {
        return srcValue;
    }
}
export const updateUser = ({
    bodymen: {
        body
    },
    params,
    user
}, res, next) => User.findById(params.id).then(notFound(res)).then((user) => user ? _.merge(user, body).save() : null).then((user) => user ? user.view(true) : null).then(success(res)).catch(next);
export const updateLocation = (req, res, next) => User.findById(req.body.id).then(notFound(res)).then((user) => user ? _.merge(user, req.body).save() : null).then((user) => user ? user.view(true) : null).then(success(res)).catch(next)
export const updatePassword = ({
    bodymen: {
        body
    },
    params,
    user
}, res, next) => User.findById(params.id === 'me' ? user._id : params.id).then(notFound(res)).then((result) => {
    if (!result) return null
    const isSelfUpdate = user._id.toString() == result._id.toString()
    if (!isSelfUpdate) {
        res.status(401).json({
            valid: false,
            param: 'password',
            message: 'You can\'t change other user\'s password'
        })
        return null
    }
    return result
}).then((user) => user ? user.set({
    password: body.password
}).save() : null).then((user) => user ? user.view(true) : null).then(success(res)).catch(next)
export const verifyAccount = ({
    params
}, res, next) => {
    User.findOne({
        verificationCode: params.key
    }).then(notFound(res)).then((user) => {
        if (!user) return null
        user.isVerified = true
        user.verificationCode = null
        return user.save()
    }).then(user => user ? user.view() : null).then(success(res)).catch(next)
}
export const destroy = ({
    params,
    user
}, res, next) => User.findById(params.id).then(notFound(res)).then((user) => user ? user.remove() : null).then(success(res, 204)).catch(next)