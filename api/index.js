import {Router} from 'express'
import user from './user'
import category from './category'
import country from './country'
import auth from './auth'
import passwordReset from './password-reset'
import chat from './chat'
import upload from './upload'
import s3 from './s3'
import service from './service'
import review from './review'
import conversation from './conversation'
import user_conversation from './user-conversation'
import offer from './offer'
import wallet from './wallet'
import charge from './charge'
import withdrawal from './withdrawal'
import serviceReport from './service-report'
const router = new Router();

/**
 * @apiDefine master Master access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine /api Admin access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine user User access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine listParams
 * @apiParam {String} [q] Query to search.
 * @apiParam {Number{1..30}} [page=1] Page number.
 * @apiParam {Number{1..100}} [limit=30] Amount of returned items.
 * @apiParam {String[]} [sort=-createdAt] Order of returned items.
 * @apiParam {String[]} [fields] Fields to be returned.
 */
router.use('/users', user);
router.use('/categories', category);
router.use('/countries', country);
router.use('/auth', auth);
router.use('/password/reset', passwordReset);
router.use('/chat', chat);
router.use('/upload', upload);
router.use('/s3', s3);
router.use('/services', service);
router.use('/reviews', review);
router.use('/conversations', conversation);
router.use('/user-conversations', user_conversation);
router.use('/offers', offer);
router.use('/wallet', wallet);
router.use('/charges', charge);
router.use('/withdrawals', withdrawal);
router.use('/service-reports', serviceReport);
export default router

