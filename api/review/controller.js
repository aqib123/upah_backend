import _ from 'lodash'
import {success, notFound} from '../../services/response/'
import { Review, User } from '../../models'
import mongoose from 'mongoose'


export const index = ({querymen: {query, select, cursor}}, res, next) =>
  Review.find(query, select, cursor).limit(4000)
    .then(success(res))
    .catch(next)


export const show = ({params}, res, next) =>
  Review.findById(params.id)
    .then(notFound(res))
    .then(success(res))
    .catch(next)

export const create = async ({body}, res, next) => {
  let reviews = await Review.find({userId: mongoose.Types.ObjectId(body.userId)});
  /*let user = await User.find({_id: mongoose.Types.ObjectId(body.userId)});
  console.log("-------------------->", user)*/

  let sumRate = 0;
  for (let i=0; i<reviews.length; i++) {
    sumRate += reviews[i].rate;
  }

  let avgRate = body.rate;
  if(reviews.length){
    sumRate = sumRate + body.rate;
    avgRate = sumRate / (reviews.length+1);
  }
  let reviewsNum = 0;
  if(reviews.length){
    reviewsNum = reviews.length
  }
  try{
    await User.findOneAndUpdate({_id: mongoose.Types.ObjectId(body.userId)}, {avgRate: avgRate, reviews: reviewsNum + 1});

  } catch(err) {console.log("=============================>", err)}
  Review.create(body)
    .then(success(res, 201))
    .catch(next)
};

export const update = ({body, user}, res, next) =>
  Review.findById(body._id)
    .then(notFound(res))
    .then((review) => review ? _.merge(review, body).save() : null)
    .then(success(res))
    .catch(next)

export const destroy = ({params, user}, res, next) =>
  Review.findById(params.id)
    .then(notFound(res))
    .then((review) => review ? review.remove() : null)
    .then(success(res, 204))
    .catch(next)



