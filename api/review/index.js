import {Router} from 'express'
import {middleware as query} from 'querymen'
import {middleware as body} from 'bodymen'
import {index, show, create, update, destroy} from './controller'
import {token} from '../../services/passport'


const router = new Router();


router.get('/',
  query(),
  index);

router.get('/:id',
  show);

router.post('/',
  token({required: true}),
  create);

router.put('/',
  update)

router.delete('/:id',
  destroy)

export default router
