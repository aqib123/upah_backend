import _ from 'lodash'
import {success, notFound} from '../../services/response/'
import { Wallet, User } from '../../models'
import mongoose from 'mongoose'

export const show = ({params}, res, next) =>{
  Wallet.findOne({bidder: mongoose.Types.ObjectId(params.id)})
    .then(notFound(res))
    .then(success(res))
    .catch(next);
};

export const create = (req, res, next) => {
  let body = {
    available: 0,
    given: 0,
    taken: 0,
    pending: 0
  }
  Wallet.create(body)
    .then(success(res, 201))
    .catch(next)
};

export const update = ({body, user}, res, next) => {
  Wallet.findById(JSON.parse(JSON.stringify(user)).wallet)
    .then(notFound(res))
    .then((wallet) => wallet ? _.merge(wallet, body).save() : null)
    .then(success(res))
    .catch(next);
};

export const updateByUser = async ({body}, res, next) => {
  let user = await User.findById(body.userId)
  Wallet.findById(user.wallet)
    .then(notFound(res))
    .then((wallet) => {
      body.taken = wallet.taken + body.taken
      body.available = wallet.available + body.taken
      return _.merge(wallet, body).save()
    })
    .then(success(res))
    .catch(next);
};
export const destroy = ({params, user}, res, next) =>
  Wallet.findById(params.id)
    .then(notFound(res))
    .then((wallet) => wallet ? wallet.remove() : null)
    .then(success(res, 204))
    .catch(next);
