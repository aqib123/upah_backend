import {Router} from 'express'
import {middleware as query} from 'querymen'
import {middleware as body} from 'bodymen'
import {show, create, update, updateByUser } from './controller'
import {token} from '../../services/passport'


const router = new Router();


router.put('/',
  token({required: true}),
  update);

router.put('/byUser',
  token({required: true}),
  updateByUser);

router.get('/',
  create);


export default router
