import { success } from '../../services/response/'
import { Category } from '../../models'


export const index = ({querymen: {query, select, cursor}}, res, next) =>
  Category.find(query, select, cursor).limit(4000)
    .then(success(res))
    .catch(next);

export const create = ({body}, res, next) => {
  body._id = body.name;
  Category.create(body)
    .then(success(res, 201))
    .catch(next)
};


