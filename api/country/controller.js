import { success } from '../../services/response/'
import { Country } from '../../models'


export const index = ({querymen: {query, select, cursor}}, res, next) =>
  Country.find(query, select, cursor).limit(4000)
    .then(success(res))
    .catch(next);
