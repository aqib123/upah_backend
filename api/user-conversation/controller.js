import {success, notFound} from '../../services/response/'
import {UConversation} from '../../models'
import _ from 'lodash'

export const create = ({body}, res, next) => {
  UConversation.create(body)
    .then(success(res, 201))
    .catch(next)
};
export const getByUser = ({params}, res, next) =>
  UConversation.find({'userId': params.userId}).populate('conversation').populate('service')
    .then(notFound(res))
    .then(success(res, 200))
    .catch(next);

export const getByUserByConversation = ({params}, res, next) =>
  UConversation.findOne({'$and': [{'userId': params.userId}, {'conversationId': params.conversationId}]})
    .then(notFound(res))
    .then(success(res, 200))
    .catch(next);

export const update = (req, res, next) => {
  UConversation.findById(req.body._id)
    .then(notFound(res))
    .then((uCon) => uCon ? _.merge(uCon, req.body).save() : null)
    .then((uCon) => uCon ? uCon.view(true) : null)
    .then(success(res))
    .catch(next)
};

export const destroy = ({params, user}, res, next) =>
    UConversation.findById(params.id)
        .then(notFound(res))
        .then((user) => user ? user.remove() : null)
        .then(success(res, 204))
        .catch(next)