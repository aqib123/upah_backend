import {Router} from 'express'
import {middleware as query} from 'querymen'
import {middleware as body} from 'bodymen'
import {getByUser, create,destroy,update, getByUserByConversation} from './controller'
import {token} from '../../services/passport'
const router = new Router();

router.get('/:userId',
token({required: true}),
  getByUser);
  
router.delete('/:id',
  token({required: true}),
  destroy);

router.get('/:userId/:conversationId',
  token({required: true}),
  getByUserByConversation);

router.post('/',
token({required: true}),
  create);

router.put('/',
  token({required: true}),
  update)
export default router
