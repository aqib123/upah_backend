/* eslint-disable no-unused-vars */
import path from 'path'
import _ from 'lodash'

var sharedConfig = require('./config.json')

const config = {
  all: {
    env: process.env.NODE_ENV || 'development',
    root: path.join(__dirname, '..'),
    port: process.env.PORT || sharedConfig.server.port,
    ip: process.env.IP || sharedConfig.server.host,
    defaultEmail: 'ibtehal990@gmail.com',
    sendgridKey: sharedConfig.SENDGRID_KEY,
    jwtSecret: sharedConfig.JWT_SECRET,
    mongo: {
      options: {
        db: {
          safe: true
        }
      }
    }
  },
  test: {
    mongo: {
      uri: sharedConfig.db.test.uri,
      options: {
        debug: false
      }
    },
    baseUrl: sharedConfig.baseUrl.test.uri
  },
  development: {
    mongo: {
      uri: sharedConfig.db.dev.uri,
      options: {
        debug: true
      }
    },
    baseUrl: sharedConfig.baseUrl.dev.uri
  },
  production: {
    ip: process.env.IP || '127.0.0.1',
    port: process.env.PORT || 9180,
    mongo: {
      uri: process.env.MONGODB_URI || sharedConfig.db.prod.uri
    },
    baseUrl: sharedConfig.baseUrl.prod.uri
  },
  external: {
    APP_TITLE: sharedConfig.appTitle,
    SERVER: {
      PROTOCOL: sharedConfig.server.protocol,
      HOST: sharedConfig.server.host,
      PORT: sharedConfig.server.port
    },
    AWS: {
      ACCESS_KEY_ID: sharedConfig.aws.accessKeyId,
      SECURET_ACCESS_KEY: sharedConfig.aws.secretAccessKey,
      REGION: sharedConfig.aws.region,
      API_VERSIONS: {
        S3: sharedConfig.aws.apiVersions.s3
      },
      BUCKET_NAME: sharedConfig.aws.bucketName,
      URL_BASIC: sharedConfig.aws.urlBasic,
      DEFAULT_SIGNED_URL_EXPIRATION_IN_SECS: sharedConfig.aws.defaultSignedUrlExpirationInSecs
    },

    SOCIAL: {
      FACEBOOK: {
        CLIENT_ID: "878833045650036",
        CLIENT_SECRET: "e82987308fd3fcc0b43e080d4f43c9b1",
      },
      GOOGLE: {
        CLIENT_ID: "762717305955-p9hrfqp2up6dfuf3kr9mobhf2f82bcd6.apps.googleusercontent.com",
        CLIENT_SECRET: "UptOkTLOmz7GlWdgNMeUh8Bv"
      }
    }
  }
}

if (!config.hasOwnProperty(config.all.env)) {
  config.all.env = 'development';
}


module.exports = _.merge(config.all, config[config.all.env], config.external)

export default module.exports
