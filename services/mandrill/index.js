import mandrill from 'mandrill-api/mandrill'
import {mandrillKey,mandrillAppPerfix, defaultEmail} from '../../config'

const mandrillClient = new mandrill.Mandrill(mandrillKey)

export const sendTemplate = ({
                               fromEmail = defaultEmail,
                               toEmail,
                               subject,
                               attachments,
                               templateName,
                               params
                             }) => {
  const promise = new Promise((resolve, reject) => {
    mandrillClient.messages.sendTemplate({
      'template_name': mandrillAppPerfix+ templateName,
      template_content: [
        {}
      ],
      message: {
        subject: subject,
        from_email: fromEmail,
        from_name: fromEmail,
        to: [
          {
            email: toEmail
          }
        ],
        attachments: attachments,
        global_merge_vars: params
      },
      'async': true
    }, function (result) {
      resolve(result)
    }, function (e) {
      console.log(e)
      reject(e)
    })
  })
  return promise
}
