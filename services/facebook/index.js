import request from 'request-promise'

export const getUser = (accessToken) =>
  request({
    uri: 'https://graph.facebook.com/me',
    json: true,
    qs: {
      access_token: accessToken,
      fields: 'id, name, email, picture, gender, first_name, last_name'
    }
  }).then(({ id, name, first_name, last_name, email, picture, gender }) => ({
    service: 'facebook',
    picture: picture.data.url,
    id,
    name,
    first_name,
    last_name,
    email,
    gender
  }));
