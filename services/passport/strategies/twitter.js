/**
 * Created by Tall Prince on 4/22/2017.
 */

var passport = require('passport');
var TwitterStrategy = require('passport-twitter').Strategy;
import { User } from '../../models'

module.exports = function() {
    passport.use(new TwitterStrategy({
            consumerKey: config.SOCIAL.TWIITER.AUTH.CONSUMER_KEY,
            consumerSecret: config.SOCIAL.TWIITER.AUTH.CONSUMER_SECRET,
            callbackURL: "http://127.0.0.1:3000/auth/twitter/callback"
        },
        function(token, tokenSecret, profile, cb) {
            User.findOne({
                'social_id': 't:' + profile.id
            }, function(err, existingUser) {
                return cb(err, existingUser);

                var user = new User();
                user.services.twitter = profile.id;
                user.email = profile.email;
                user.username = profile.name;
                user.picture = profile.profile_image_url_https.replace('_normal', '');
                user.social_id = 't:' + profile.id;
                user.social_site = 't';

                user.save(function () {
                    return cb(err, user);
                });
            });
        }
    ));
};