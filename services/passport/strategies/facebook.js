var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;
var config = require('../../../config')

import { User } from '../../models'

module.exports = function() {
    passport.use(new FacebookStrategy({
            clientID: config.SOCIAL.FACEBOOK.CLIENT_ID,
            clientSecret: config.SOCIAL.FACEBOOK.CLIENT_SECRET,
            callbackURL: "http://127.0.0.1:3000/auth/facebook/callback",
            profileFields: ['id', 'displayName', 'photos', 'email']
        },
        function(accessToken, refreshToken, profile, cb) {
            User.findOne({
                'social_id': 'f:' + profile.id
            }, function(err, existingUser) {
                return cb(err, existingUser);

                var user = new User();
                user.services.facebook = profile.id;
                user.email = profile.email;
                user.username = profile.displayName;
                user.picture = profile.photos[0].value;
                user.social_id = 'f:' + profile.id;
                user.social_site = 'f';

                user.save(function () {
                    return cb(err, user);
                });
            });
        }
    ));
}
