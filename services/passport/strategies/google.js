/**
 * Created by Tall Prince on 4/22/2017.
 */
var passport = require('passport');
var GoogleStrategy = require( 'passport-google-oauth2' ).Strategy;
var config = require('../../../config')

import { User } from '../../models'

module.exports = function() {
    passport.use(new GoogleStrategy({
            clientID:     config.SOCIAL.GOOGLE.CLIENT_ID,
            clientSecret: config.SOCIAL.GOOGLE.CLIENT_SECRET,
            callbackURL: "http://127.0.0.1:3000/auth/google/callback",
            passReqToCallback   : true
        },
        function(request, accessToken, refreshToken, profile, done) {
            User.findOne({
                'social_id': 'g:' + profile.id
            }, function(err, existingUser) {
                return cb(err, existingUser);

                var user = new User();
                user.services.google = profile.id;
                user.email = profile.email;
                user.username = profile.displayName;
                user.picture = profile.photos[0].value;
                user.social_id = 'g:' + profile.id;
                user.social_site = 'g';

                user.save(function () {
                    return cb(err, user);
                });
            });
        }
    ));
};