var passport = require('passport');

module.exports = function() {
    require('./facebook.js')();
    require('./google.js')();
    require('./twitter.js')();

    passport.serializeUser(function(user, done) {
        done(null, user.social_id);
    });
    passport.deserializeUser(function(id, done) {
        User.findOne({
            social_id: id
        }, '-password -salt', function(err, user) {
            done(err, user);
        });
    });
}
