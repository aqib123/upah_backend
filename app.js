import http from "http";
import {env, ip, mongo} from "./config";
import mongoose from "./services/mongoose";
import express from "./services/express";
import api from "./api";
import {Router} from "express";

const combinedRouter = new Router();

combinedRouter.use('/api/v1/', api);

const app = express(combinedRouter);
const server = http.createServer(app);
const port = process.env.PORT || 9002;
var io = require("socket.io").listen(server);
io.on('connection', function(socket){
  socket.on('room', function(room) {
    socket.join(room);
  });

  socket.on('chating', function(data){
    //join room with conversation id
    io.sockets.in(data.conversationId).emit('chating', data);
  });
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});


// mongoose.connect(mongo.uri);
mongoose.connect("mongodb://localhost:27017/upah-dev");
// mongoose.connect("mongodb://aqib:PeoplePerHour1@ds231199.mlab.com:31199/heroku_3fv29khc");

setImmediate(() => {
  server.listen(process.env.PORT || 9002, () => {
    console.log('Express server listening on http://%s:%d, in %s mode', ip, port, env)
    console.log('MongoDB on %s', mongo.uri)
  })
});

export default app
