import mongoose, {Schema} from 'mongoose'

const withdrawSchema = new Schema({
  userId: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  stripeCustomerId: {
    type: String
  },
  userName: {
    type: String
  },
  status: {
    type: String
  },
  amount: {
  type: Number
}
}, {
  timestamps: true
});

withdrawSchema.methods = {
  view (full) {
    let view = {};
    let fields = ['stripeCustomerId','userId', 'userName', 'amount', status];
    if (full) {
      fields = [...fields,'createdAt']
    }
    fields.forEach((field) => {
      view[field] = this[field]
    });
    return view
  }
};

export const model = mongoose.model('Withdrawal', withdrawSchema);

export const schema = model.schema;
export default model
