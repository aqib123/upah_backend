import mongoose, {Schema} from 'mongoose'

const serviceSchema = new Schema({
  type: {
    type: String,
    required: true
  },
  description: {
    type: String,
  },
  categoryId: {
    type: Schema.ObjectId,
    ref: 'Category'
  },
  location: {
    type: String
  },
  userId: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  workerId: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  price: {
    type: Number
  },
  status: {
    type: String
  },
  date: {
    type: String
  },
  upah: {
  },
  photos: [{
    type: String
  }],
  deliveryAddress: {
    type: String
  }

}, {
  timestamps: true
});

serviceSchema.methods = {
  view () {
    let view = {};
    let fields = ['price', 'upah', 'deliverAddress', 'photos', 'type', 'userId', 'categoryId', 'description',
      'location', 'status', 'workerId'];
    fields.forEach((field) => {
      view[field] = this[field]
    });
    return view
  }
};

export const model = mongoose.model('Service', serviceSchema)

export const schema = model.schema
export default model
