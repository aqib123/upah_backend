import mongoose, {Schema} from 'mongoose'

const userConversationSchema = new Schema({
  userId: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  conversationId: {
    type: Schema.ObjectId,
    ref: 'Conversation'
  },
  service: {
    type: Schema.ObjectId,
    ref: 'Service'
  },
  readMessages: {
    type: Number
  },
}, {
  timestamps: true
});

userConversationSchema.methods = {
  view (full) {
    let view = {};
    let fields = ['id', 'readMessages','userId', 'conversationId', 'service'];
    if (full) {
      fields = [...fields,'createdAt']
    }
    fields.forEach((field) => {
      view[field] = this[field]
    });
    return view
  }
};

export const model = mongoose.model('UserConversation', userConversationSchema);

export const schema = model.schema;
export default model
