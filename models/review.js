import mongoose, {Schema} from 'mongoose'

const reviewSchema = new Schema({
  rate: {
    type: Number,
    required: true
  },
  userId: {
    type: Schema.ObjectId,
    ref: 'User',
  },
  serviceId: {
    type: Schema.ObjectId,
    ref: 'Service',
  },

}, {
  timestamps: true
})

reviewSchema.methods = {
  view () {
    let view = {};
    let fields = ['rate', 'userId', 'serviceId'];
    fields.forEach((field) => {
      view[field] = this[field]
    });
    return view
  }
};

export const model = mongoose.model('Review', reviewSchema)

export const schema = model.schema
export default model
