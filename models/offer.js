import mongoose, {Schema} from 'mongoose'

const offerSchema = new Schema({
  status: { type: String },
  owner: {
    type: Schema.ObjectId,
    ref: 'User',
  },
  bidder: {
    type: Schema.ObjectId,
    ref: 'User',
  },
  service: {
    type: Schema.ObjectId,
    ref: 'Service',
  },
  upah: { type: Number }
}, {
  timestamps: true
});

offerSchema.methods = {
  view (full) {
    let view = {};
    let fields = ['status','bidder','owner','service','upah'];
    if (full) {
      fields = [...fields,'createdAt']
    }
    fields.forEach((field) => {
      view[field] = this[field]
    });
    return view
  }
};

export const model = mongoose.model('Offer', offerSchema);

export const schema = model.schema;
export default model
