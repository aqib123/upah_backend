import mongoose, {Schema} from 'mongoose'

const countrySchema = new Schema({
  name:{
    type: String
  },
  dial_code:{
    type: String
  },
  code:{
    type: String
  }
}, {
  timestamps: true
});

countrySchema.methods = {
  view (full) {
    let view = {};
    let fields = ['id', 'name', 'dial_code', 'code'];

    if (full) {
      fields = [...fields]
    }

    fields.forEach((field) => {
      view[field] = this[field]
    });
    return view
  }
};


const model = mongoose.model('Country', countrySchema);
const schema = model.schema;

export { model, schema }
