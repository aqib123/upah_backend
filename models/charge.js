import mongoose, {Schema} from 'mongoose'

const chargeSchema = new Schema({
  amount: {
    type: Number
  },
  currency: {
    type: String
  },
  senderId: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  receiverId: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  senderStripeId: {
    type: String
  },
  receiverStripeId: {
    type: String
  }
}, {
  timestamps: true
});

chargeSchema.methods = {
  view (full) {
    let view = {};
    let fields = ['amount','currency','receiverId','senderId', 'senderStripeId', 'receiverStripeId'];
    if (full) {
      fields = [...fields,'createdAt']
    }
    fields.forEach((field) => {
      view[field] = this[field]
    });
    return view
  }
};

export const model = mongoose.model('Charge', chargeSchema);

export const schema = model.schema;
export default model
