import mongoose, {Schema} from 'mongoose'

const serviceReportSchema = new Schema({
  reporterId:{
    type: String
  },
  serviceId:{
    type: String
  },
  userId: {
    type: String
  }
}, {
  timestamps: true
});

serviceReportSchema.methods = {
  view (full) {
    let view = {};
    let fields = ['userId', 'serviceId', 'reporterId'];

    if (full) {
      fields = [...fields]
    }

    fields.forEach((field) => {
      view[field] = this[field]
    });
    return view
  }
};


const model = mongoose.model('ServiceReport', serviceReportSchema);
const schema = model.schema;

export { model, schema }
