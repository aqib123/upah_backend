import mongoose, { Schema } from 'mongoose'
import { uid } from 'rand-token'
const crypto = require('crypto');

const passwordResetSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    index: true
  },
  code: {
    type: String,
    unique: true,
    index: true,
    default: () => uid(4)
},
  createdAt: {
    type: Date,
    default: Date.now,
    expires: 3600
  }
});

passwordResetSchema.methods = {
  view (full) {
    return {
      user: this.user.view(full),
      token: this.token
    }
  }
}

export const model = mongoose.model('PasswordReset', passwordResetSchema)

export const schema = model.schema
export default model
