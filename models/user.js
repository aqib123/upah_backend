import crypto from 'crypto'
import bcrypt from 'bcrypt'
import randtoken from 'rand-token'
import mongoose, {
    Schema
} from 'mongoose'
import {
    uid
} from 'rand-token'
import {
    env
} from '../config'
const userSchema = new Schema({
    email: {
        type: String,
        match: /^\S+@\S+\.\S+$/,
        required: true,
        unique: true,
        trim: true,
        lowercase: true
    },
    wallet: {
        type: Schema.ObjectId,
        ref: 'Wallet'
    },
    stripeCustomerId: {
        type: String
    },
    stripeSetup: {
        type: Boolean
    },
    isVerified: {
        type: Boolean,
        default: false,
    },
    verificationCode: {
        type: String,
        default: () => uid(32)
    },
    password: {
        type: String,
        minlength: 6
    },
    firstName: {
        type: String
    },
    lastName: {
        type: String,
        index: true,
        trim: true
    },
    gender: {
        type: String,
        enum: ['Male', 'Female'],
        default: 'Male'
    },
    dob: {
        type: Date,
    },
    country: {
        type: String,
        trim: true
    },
    contactNo: {
        type: String,
        trim: true
    },
    transport: {
        type: [String]
    },
    area: {
        type: String,
        trim: true
    },
    acceptedTerms: {
        type: Boolean
    },
    lat: {
        type: String
    },
    lng: {
        type: String
    },
    picture: {
        type: String,
        trim: true
    },
    services: {
        facebook: String,
        google: String
    },
    avgRate: {
        type: Number,
        default: 0
    },
    reviews: {
        type: Number,
        default: 0
    },
    upah: {
        type: Number,
        default: 0
    },
    bankAccountNumber: {
        type: String,
        trim: true
    }
}, {
    timestamps: true
});
userSchema.path('email').set(function(email) {
    if (!this.picture) {
        this.picture = "https://firebasestorage.googleapis.com/v0/b/flamelink-project.appspot.com/o/upah-images%2FOf1XZCh7.jpg?alt=media&token=9bc56dc6-f4e8-4609-83d0-0e4e6a41efab"
    }
    return email
});
userSchema.pre('save', function(next) {
    if (!this.isModified('password')) return next()
    /* istanbul ignore next */
    const rounds = env === 'test' ? 1 : 9
    bcrypt.hash(this.password, rounds).then((hash) => {
        this.password = hash
        next()
    }).catch(next)
});
userSchema.statics = {
    //roles,
    createFromService({
        service,
        id,
        email,
        name,
        first_name,
        last_name,
        picture,
        gender,
        wallet
    }) {
        console.log("===================>", service, id, email, name, picture, gender)
        return this.findOne({
            $or: [{
                [`services.${service}`]: id
            }, {
                email
            }]
        }).then((user) => {
            if (user) {
                user.services[service] = id;
                if (name) user.firstName = name;
                user.firstName = first_name;
                user.lastName = last_name;
                user.picture = picture;
                user.isVerified = true;
                user.wallet = wallet;
                return user.save()
            } else {
                const password = randtoken.generate(16);
                var hasEmail = email ? true : false;
                if (!hasEmail) email = 'none@example.com';
                return this.create({
                    services: {
                        [service]: id
                    },
                    email,
                    password,
                    first_name,
                    last_name,
                    picture,
                    wallet,
                    verificationCode: null,
                    isVerified: hasEmail
                })
            }
        })
    }
}
userSchema.methods = {
    view(full) {
        let view = {};
        let fields = ['id', 'firstName', 'picture', 'lastName', 'stripeCustomerId', 'stripeSetup', 'readMessages', 'gender', 'wallet', 'dob', 'email', 'area', 'transport', 'reviews', 'avgRate', 'upah', 'country', 'contactNo', 'createdAt', 'isVerified', 'role', 'acceptedTerms', 'lat', 'lng', 'bankAccountNumber'];
        if (full) {
            fields = [...fields, 'gender', 'wallet', 'dob', 'email', 'area', 'transport', 'createdAt', 'isVerified', 'role', 'acceptedTerms', 'lat', 'lng', 'bankAccountNumber']
        }
        fields.forEach((field) => {
            view[field] = this[field]
        });
        return view
    },
    authenticate(password) {
        return bcrypt.compare(password, this.password).then((valid) => valid ? this : false)
    }
};
const model = mongoose.model('User', userSchema)
const schema = model.schema
export {
    model,
    schema
}