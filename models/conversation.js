import mongoose, {Schema} from 'mongoose'

const conversationSchema = new Schema({
  senderId: {
    type: Schema.ObjectId,
    ref: 'User',
    index: true
  },
  receiverId: {
    type: Schema.ObjectId,
    ref: 'User',
    index: true
  },
  serviceId: {
    type: Schema.ObjectId,
    ref: 'Service',
    index: true
  },
}, {
  timestamps: true
});

conversationSchema.methods = {
  view (full) {
    let view = {};
    let fields = ['id', 'senderId','receiverId', 'serviceId'];
    if (full) {
      fields = [...fields,'createdAt']
    }
    fields.forEach((field) => {
      view[field] = this[field]
    });
    return view
  }
};

export const model = mongoose.model('Conversation', conversationSchema)

export const schema = model.schema
export default model
