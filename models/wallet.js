import mongoose, {Schema} from 'mongoose'

const walletSchema = new Schema({
  available: { type: Number },
  given: {
    type: Number
  },
  taken: {
    type: Number
  },
  pending: {
    type: Number
  }
}, {
  timestamps: true
});

walletSchema.methods = {
  view (full) {
    let view = {};
    let fields = ['available','given','taken','pending'];
    if (full) {
      fields = [...fields,'createdAt']
    }
    fields.forEach((field) => {
      view[field] = this[field]
    });
    return view
  }
};

export const model = mongoose.model('Wallet', walletSchema);

export const schema = model.schema;
export default model
