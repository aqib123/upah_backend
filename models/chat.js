import mongoose, {Schema} from 'mongoose'

const chatSchema = new Schema({
  chat_id: { type: Number },
  message: { type: String },
  senderId: {
    type: Schema.ObjectId,
    ref: 'User',
    index: true
  },
  type: {
    type: String,
    default: 'message'
  },
  receiverId: {
    type: Schema.ObjectId,
    ref: 'User',
    index: true
  },
  conversationId: {
    type: Schema.ObjectId,
    ref: 'Conversation',
    index: true
  },
}, {
  timestamps: true
});

chatSchema.methods = {
  view (full) {
    let view = {};
    let fields = ['chat_id','message','senderId','receiverId','conversationId', 'type', 'createdAt'];
    if (full) {
      fields = [...fields,'createdAt']
    }
    fields.forEach((field) => {
      view[field] = this[field]
    });
    return view
  }
};

export const model = mongoose.model('Chat', chatSchema);

export const schema = model.schema;
export default model
