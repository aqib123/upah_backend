import mongoose, {Schema} from 'mongoose'

const categorySchema = new Schema({
  name:{
    type:String
  }
}, {
  timestamps: true
});

categorySchema.methods = {
  view (full) {
    let view = {}
    let fields = ['id', 'name'];

    if (full) {
      fields = [...fields,'name','createdAt']
    }

    fields.forEach((field) => {
      view[field] = this[field]
    });
    return view
  },
}


const model = mongoose.model('Category', categorySchema)
const schema = model.schema

export { model, schema }
